import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ara'
})
export class AraPipe implements PipeTransform {

  transform(products: Array<any>, searchTerm?:string): any {
    //console.log("value: ",products, "args",searchTerm);
    if(!searchTerm){
      return products;
    }{
      return products.filter(p=>p.name.toLowerCase().indexOf(searchTerm.toLowerCase())>=0)
    }
  }

}
