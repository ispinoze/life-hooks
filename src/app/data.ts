export const data=[{
    "id": 1,
    "createdAt": "13.07.2017",
    "name": "Appetizer - Lobster Phyllo Roll",
    "price": "$0.71",
    "vendor": "Thoughtstorm",
    "stock": 67,
    "productCode": "a4ec1a60-bd3c-4cc0-b150-bbb75eb000fa"
  }, {
    "id": 2,
    "createdAt": "20.06.2017",
    "name": "Dill Weed - Dry",
    "price": "$0.88",
    "vendor": "Podcat",
    "stock": 42,
    "productCode": "a7dda262-03aa-496f-9bb6-a53115911606"
  }, {
    "id": 3,
    "createdAt": "14.07.2017",
    "name": "Cream Of Tartar",
    "price": "$7.73",
    "vendor": "Mynte",
    "stock": 2,
    "productCode": "7a831cd9-a988-4143-8ea5-c6b6cf881ed9"
  }, {
    "id": 4,
    "createdAt": "28.02.2017",
    "name": "Bamboo Shoots - Sliced",
    "price": "$5.47",
    "vendor": "Mita",
    "stock": 72,
    "productCode": "b11af983-839d-4016-b710-c4168e23048e"
  }, {
    "id": 5,
    "createdAt": "08.09.2017",
    "name": "Corn Kernels - Frozen",
    "price": "$7.96",
    "vendor": "Rhybox",
    "stock": 91,
    "productCode": "1e0a1943-2c5d-4235-9074-77503b2a8a1c"
  }, {
    "id": 6,
    "createdAt": "10.11.2016",
    "name": "Coconut - Creamed, Pure",
    "price": "$9.19",
    "vendor": "Gabspot",
    "stock": 85,
    "productCode": "812754f8-ba44-4f2a-8528-4c89fd690af0"
  }, {
    "id": 7,
    "createdAt": "30.11.2016",
    "name": "Cheese - Oka",
    "price": "$9.17",
    "vendor": "Photobug",
    "stock": 83,
    "productCode": "0c8e0904-22c5-42d2-9e92-997985818d11"
  }, {
    "id": 8,
    "createdAt": "01.08.2017",
    "name": "Flour - Semolina",
    "price": "$4.91",
    "vendor": "Babbleopia",
    "stock": 40,
    "productCode": "cde53ecc-a6b1-4d49-b4c0-9ae747097b3c"
  }, {
    "id": 9,
    "createdAt": "30.10.2016",
    "name": "Butter - Salted",
    "price": "$2.89",
    "vendor": "Innotype",
    "stock": 55,
    "productCode": "00d5b810-417a-47d0-af9e-52c20b5df05f"
  }, {
    "id": 10,
    "createdAt": "11.01.2017",
    "name": "Wine - Red, Black Opal Shiraz",
    "price": "$6.38",
    "vendor": "Skivee",
    "stock": 19,
    "productCode": "47ba328b-2e03-49df-9eda-6d1e5adc1b7b"
  }]