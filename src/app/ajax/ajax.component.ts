import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ajax',
  templateUrl: './ajax.component.html',
  styleUrls: ['./ajax.component.css']
})
export class AjaxComponent implements OnInit {

  constructor(private route:ActivatedRoute,private router:Router) { }

  params;
  ver;
  queryParams;
  snapShot;

  ngOnInit() {
    this.route.params
    .subscribe(_par=>{
      this.params=_par
      this.ver=_par['version']
    })
    this.route.queryParams
    .subscribe(_par=>{
      this.queryParams=_par
    })

    this.snapShot=this.route.snapshot.queryParams
  }

}
