import { Component, OnInit,Input  } from '@angular/core';

import {DomSanitizer} from '@angular/platform-browser'

@Component({
  selector: 'app-html-show',
  templateUrl: './html-show.component.html',
  styleUrls: ['./html-show.component.css']
})
export class HtmlShowComponent implements OnInit {

  @Input('html') html

  constructor(private ds:DomSanitizer) { }

  ngOnInit() {
  }

  sanitize(icerik:string){
    return this.ds.bypassSecurityTrustHtml(icerik);
  }

}
