import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlShowComponent } from './html-show.component';

describe('HtmlShowComponent', () => {
  let component: HtmlShowComponent;
  let fixture: ComponentFixture<HtmlShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
