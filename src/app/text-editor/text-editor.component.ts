import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';

declare var tinymce:any;

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.css']
})
export class TextEditorComponent implements OnInit {

  @Input('editorId') editorId

  @Output() editorContentEvent=new EventEmitter<any>();
  
  editor;
  
  constructor() { }
  ngOnInit(){ }

  ngAfterViewInit() {
    tinymce.init({
      selector: '#'+this.editorId,
      skin_url: 'assets/skins/lightgray',
      plugins: ['link', 'paste', 'table'],
      setup:(editor)=>{
        this.editor=editor
        editor.on('keyUp',()=>{
          const content=editor.getContent()
          const contentRaw=editor.getContent({format:'text'})
          this.editorContentEvent.emit({content,contentRaw});
        })
      }
    })
  }

  ngOnDestroy(){
    tinymce.remove();
  }
}
