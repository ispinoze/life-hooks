import { AjaxComponent } from './ajax/ajax.component';
import { FeRoutingComponent } from './fe-routing/fe-routing.component';
import { UstMenuComponent } from './ust-menu/ust-menu.component';
import { HtmlComponent } from './html/html.component';
import { HomeComponent } from './home/home.component';
import {Routes} from "@angular/router"

export const appRoutes:Routes=[
    {
        path:'',component:HomeComponent
    },
    {
        path:'http',component:HtmlComponent
    },
    {
        path:'di',component:HtmlComponent
    },
    {
        path:'routing/:version/:lang',component:FeRoutingComponent,
        children:[
            {path:'ajax',component:AjaxComponent}
        ]
    }
]