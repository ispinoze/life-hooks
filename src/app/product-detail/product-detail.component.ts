import { Component, OnInit,Input,EventEmitter,Output } from '@angular/core';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {


  @Input('selectedProductInput') selectedProductInput;

  @Output() closeEvent=new EventEmitter;

  constructor() { }

  ngOnInit() {
  }

  kapat(){
    this.closeEvent.emit()
;  }
}
