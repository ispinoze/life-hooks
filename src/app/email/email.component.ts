import { Component, OnInit, NgZone } from '@angular/core';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {

  htmlFromEditor='İçerik Yok'

  constructor(private zone:NgZone) { }

  ngOnInit() {
  }

  editorIcerikDegisim($event){
    this.zone.run(()=>{
      this.htmlFromEditor=$event.content;  
    })
  }
}
