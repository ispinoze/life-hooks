import { appRoutes } from './app.routes';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { LifeHooksComponent } from './life-hooks/life-hooks.component';
import { DemoComponent } from './demo/demo.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { AraPipe } from './ara.pipe';
import { EmailComponent } from './email/email.component';
import { TextEditorComponent } from './text-editor/text-editor.component';
import { HtmlShowComponent } from './html-show/html-show.component';
import { SimpleTinyComponent } from './simple-tiny/simple-tiny.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from "@angular/router";
import { HtmlComponent } from './html/html.component';
import { UstMenuComponent } from './ust-menu/ust-menu.component';
import { FeRoutingComponent } from './fe-routing/fe-routing.component';
import { AjaxComponent } from './ajax/ajax.component';

@NgModule({
  declarations: [
    AppComponent,
    LifeHooksComponent,
    DemoComponent,
    ProductListComponent,
    ProductDetailComponent,
    AraPipe,
    EmailComponent,
    TextEditorComponent,
    HtmlShowComponent,
    SimpleTinyComponent,
    HomeComponent,
    HtmlComponent,
    UstMenuComponent,
    FeRoutingComponent,
    AjaxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
