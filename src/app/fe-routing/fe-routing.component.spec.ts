import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeRoutingComponent } from './fe-routing.component';

describe('FeRoutingComponent', () => {
  let component: FeRoutingComponent;
  let fixture: ComponentFixture<FeRoutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeRoutingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeRoutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
