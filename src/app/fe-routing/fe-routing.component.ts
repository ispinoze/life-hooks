import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-fe-routing',
  templateUrl: './fe-routing.component.html',
  styleUrls: ['./fe-routing.component.css']
})
export class FeRoutingComponent implements OnInit {

  constructor(private route:ActivatedRoute,private router:Router) { }

  params;
  queryParams;
  ver;

  snapShot;

  ngOnInit() {
    this.route.params
      .subscribe(_par=>{
        this.params=_par
        this.ver=_par['version']
      })
      this.route.queryParams
      .subscribe(_par=>{
        this.queryParams=_par
      })

      this.snapShot=this.route.snapshot.queryParams
  }


  navigate(){
    this.router.navigate(['routing','v5','java',{m:5,debug:true}],{queryParams:{q1:12,q2:'Ali'}});
  }

}
