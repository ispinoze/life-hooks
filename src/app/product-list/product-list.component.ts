import { Component, OnInit,Input,EventEmitter,Output,SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  @Input('productsInput') productsInput

  @Output() productEvent=new EventEmitter<any>()

  selectedProduct;

  degisiklikListesi=[];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes:SimpleChanges){

    for(let prop in changes){

      let degisen = changes[prop]

      if(!degisen.firstChange){
        let current=JSON.stringify(degisen.currentValue);
        let prev=JSON.stringify(degisen.previousValue);
        this.degisiklikListesi.push(`${prop}, Önceki Değeri:${prev}, Şimdiki Değeri:${current}`);
      }
    }
  }

  setSelected(product){
    this.productEvent.emit(product);
  }

}
