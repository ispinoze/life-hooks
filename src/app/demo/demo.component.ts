import { Component, OnInit } from '@angular/core';
import { EventManager } from '@angular/platform-browser';


import {data} from '../data'

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

  products=[]
  
  selectedProduct;

  searchStart=false

  secondSearchStart=false

  constructor(private ev:EventManager) { }

  ngOnInit() {
    this.products=data;
    this.ev.addGlobalEventListener("window","keydown.alt.s",()=>{
      this.secondSearchStart=!this.secondSearchStart  
    });

    this.ev.addGlobalEventListener("window","keyup.esc",()=>{
      this.secondSearchStart=false
    });
  }

  setSelected(product){
    this.selectedProduct=product;
  }

  searchOnChange(searchTerm){
    //this.products=this.products.filter(p=>p.name.toLowerCase().indexOf(searchTerm)>=0)
  }

  internetSearchStarted(p){
    this.secondSearchStart=!this.secondSearchStart
  }

}
